import java.util.Scanner;

public class Main {

    /**
     * Entry point for this project.
     *
     * The main method create an instance of the players, the game and displays the scores.
     */
    public static void main(String[] args) {
        String response;

        // ***********************************************************************
        // The do-while loop is for re-asking the player if he want to play again.
        // ***********************************************************************
        do {
            Player player1 = new Player("Spieler A");
            Player player2 = new Player("Spieler B");
            Battle battle = new Battle();

            for (int i=0; i<100; i++) {
                player1.setSelectedStrategy("papier");
                player2.selectAStrategy(new Scanner(System.in));
                battle.battle(player1, player2);
            }
            // **************************************************
            // Here is displayed the different score of the game.
            // **************************************************
            System.out.println(player1.getName() + " gewinnt " + battle.getScoreOfPlayer1() + " von 100 Spielen !");
            System.out.println(player2.getName() + " gewinnt " + battle.getScoreOfPlayer2() + " von 100 Spielen !");
            System.out.println("Unentschieden: " + battle.getScoreOfEquality() + " von 100 Spielen !\n");

            System.out.println("Wollen Sie nochmal spielen ? (Drücken Sie j für Ja oder eine etwas anderes für Nein) : ");
            Scanner scanner = new Scanner(System.in);
            response = scanner.nextLine();
        } while (response.equals("j"));
    }
}
