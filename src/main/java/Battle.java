public class Battle {

    private int scoreOfPlayer1;
    private int scoreOfPlayer2;
    private int scoreOfEquality;

    /**
    * Default constructor
    */
    public Battle() {}

    /**
     * Public methods battle
     * Takes into parameter two players and tests all the possibilities of choice of strategy in order to determine the score
     */
    public void battle(Player player1, Player player2) {
        if (player1.getSelectedStrategy().equals(player2.getSelectedStrategy())) {
            scoreOfEquality++;
        } else if ("schere".equals(player2.getSelectedStrategy()) && "papier".equals(player1.getSelectedStrategy())) {
            scoreOfPlayer2++;
        } else if ("schere".equals(player2.getSelectedStrategy()) && "stein".equals(player1.getSelectedStrategy())) {
            scoreOfPlayer1++;
        } else if ("stein".equals(player2.getSelectedStrategy()) && "papier".equals(player1.getSelectedStrategy())) {
            scoreOfPlayer1++;
        } else if ("stein".equals(player2.getSelectedStrategy()) && "schere".equals(player1.getSelectedStrategy())) {
            scoreOfPlayer2++;
        } else if ("papier".equals(player2.getSelectedStrategy()) && "schere".equals(player1.getSelectedStrategy())) {
            scoreOfPlayer1++;
        } else if ("papier".equals(player2.getSelectedStrategy()) && "stein".equals(player1.getSelectedStrategy())) {
            scoreOfPlayer2++;
        }
    }

    public int getScoreOfPlayer1() {
        return scoreOfPlayer1;
    }

    public int getScoreOfPlayer2() {
        return scoreOfPlayer2;
    }

    public int getScoreOfEquality() {
        return scoreOfEquality;
    }
}
