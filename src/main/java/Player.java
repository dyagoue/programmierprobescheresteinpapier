import java.util.Scanner;

public class Player {

    private String name;
    private String selectedStrategy;
    private int i;

    /**
     * Parameterized constructor.
     * @param name The name that will be assigned to the player.
     */
    public Player(String name) { this.name = name; }

    /**
     * Public method selectAStrategy.
     * Retrieves the player's choice of strategy.
     */
    public void selectAStrategy(Scanner scanner) {

        // ***************************************************************************************************
        // The do-while loop is for re-asking the player to enter the correct name as long he makes a mistake.
        // ***************************************************************************************************
        do {
            System.out.print(name + " bitte wählen Sie eine Strategie (schere, stein oder papier): ");
            String strategy = scanner.nextLine();

            // *****************************************************************
            // Check if the player has entered the correct name of the strategy.
            // *****************************************************************
            if ("schere".equals(strategy) || "stein".equals(strategy) || "papier".equals(strategy)) {
                this.setSelectedStrategy(strategy);
                i = 1;
            } else {
                // ************************************************************************
                // Ask the player to rewrite the name of the strategy in case of a mistake.
                // ************************************************************************
                System.out.println(name + " bitte geben Sie den korrekten Namen der Strategie ein !");
                i = 0;
            }
        } while (i==0);
    }

    public String getName() {
        return name;
    }

    public String getSelectedStrategy() {
        return selectedStrategy;
    }

    public void setSelectedStrategy(String selectedStrategy) {
        this.selectedStrategy = selectedStrategy;
    }

    public int getI() {
        return i;
    }

}
