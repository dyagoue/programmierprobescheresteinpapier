import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BattleTest {
    Player player1 = new Player("player1");
    Player player2 = new Player("player2");
    Battle battle = new Battle();

    private int testScoreOfPlayer1;
    private int testScoreOfPlayer2;
    private int testScoreOfEquality;

    @Test
    public void testIfCondition1() {
        player1.setSelectedStrategy("papier");
        player2.setSelectedStrategy("papier");
        battle.battle(player1, player2);
        testScoreOfEquality = 1;
        assertEquals(battle.getScoreOfEquality(), testScoreOfEquality);
    }

    @Test
    public void testIfCondition2() {
        player1.setSelectedStrategy("papier");
        player2.setSelectedStrategy("schere");
        battle.battle(player1, player2);
        testScoreOfPlayer2 = 1;
        assertEquals(battle.getScoreOfPlayer2(), testScoreOfPlayer2);
    }

    @Test
    public void testIfCondition3() {
        player1.setSelectedStrategy("stein");
        player2.setSelectedStrategy("schere");
        battle.battle(player1, player2);
        testScoreOfPlayer1 = 1;
        assertEquals(battle.getScoreOfPlayer1(), testScoreOfPlayer1);
    }

    @Test
    public void testIfCondition4() {
        player1.setSelectedStrategy("papier");
        player2.setSelectedStrategy("stein");
        battle.battle(player1, player2);
        testScoreOfPlayer1 = 1;
        assertEquals(battle.getScoreOfPlayer1(), testScoreOfPlayer1);
    }

    @Test
    public void testIfCondition5() {
        player1.setSelectedStrategy("schere");
        player2.setSelectedStrategy("stein");
        battle.battle(player1, player2);
        testScoreOfPlayer2 = 1;
        assertEquals(battle.getScoreOfPlayer2(), testScoreOfPlayer2);
    }

    @Test
    public void testIfCondition6() {
        player1.setSelectedStrategy("schere");
        player2.setSelectedStrategy("papier");
        battle.battle(player1, player2);
        testScoreOfPlayer1 = 1;
        assertEquals(battle.getScoreOfPlayer1(), testScoreOfPlayer1);
    }

    @Test
    public void testIfCondition7() {
        player1.setSelectedStrategy("stein");
        player2.setSelectedStrategy("papier");
        battle.battle(player1, player2);
        testScoreOfPlayer2 = 1;
        assertEquals(battle.getScoreOfPlayer2(), testScoreOfPlayer2);
    }
}
