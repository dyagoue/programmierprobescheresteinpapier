import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class PlayerTest {
    Player player = new Player("player");
    private String testStrategy;

    @Test
    public void testSelectAStrategyIfCase(){
        testStrategy = "papier";
        Scanner testScanner = new Scanner(testStrategy);
        player.selectAStrategy(testScanner);
        assertEquals(player.getI(), 1);

    }

    @Test
    public void testSelectAStrategyElseCase(){
        testStrategy = "jhjhgv";
        Scanner testScanner = new Scanner(testStrategy);
        try {
            player.selectAStrategy(testScanner);
        } catch (NoSuchElementException e) { }
        assertEquals(player.getI(), 0);
    }

    @Test
    public void testGetName(){
        assertEquals(player.getName(), "player");
    }

    @Test
    public void testGetSelected(){
        player.setSelectedStrategy("stein");
        assertEquals(player.getSelectedStrategy(), "stein");
    }
}
